# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2016, 2017, 2018, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-29 00:46+0000\n"
"PO-Revision-Date: 2022-07-22 15:05+0100\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-l10n-en_gb@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.3\n"

#: package/contents/ui/Battery.qml:26
#, kde-format
msgid "%1% charging"
msgstr "%1% charging"

#: package/contents/ui/Battery.qml:26
#, kde-format
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/Battery.qml:26
#, kde-format
msgid "No info"
msgstr "No info"

#: package/contents/ui/Connectivity.qml:41
#, kde-format
msgid "Unknown"
msgstr "Unknown"

#: package/contents/ui/Connectivity.qml:51
#, kde-format
msgid "No signal"
msgstr "No signal"

#: package/contents/ui/DeviceDelegate.qml:51
#, kde-format
msgid "File Transfer"
msgstr "File Transfer"

#: package/contents/ui/DeviceDelegate.qml:52
#, kde-format
msgid "Drop a file to transfer it onto your phone."
msgstr "Drop a file to transfer it onto your phone."

#: package/contents/ui/DeviceDelegate.qml:87
#, kde-format
msgid "Virtual Display"
msgstr "Virtual Display"

#: package/contents/ui/DeviceDelegate.qml:138
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/DeviceDelegate.qml:160
#, kde-format
msgid "Please choose a file"
msgstr "Please choose a file"

#: package/contents/ui/DeviceDelegate.qml:169
#, kde-format
msgid "Share file"
msgstr "Share file"

#: package/contents/ui/DeviceDelegate.qml:178
#, kde-format
msgid "Save As"
msgstr ""

#: package/contents/ui/DeviceDelegate.qml:191
#, kde-format
msgid "Take a photo"
msgstr ""

#: package/contents/ui/DeviceDelegate.qml:206
#, kde-format
msgid "Ring my phone"
msgstr "Ring my phone"

#: package/contents/ui/DeviceDelegate.qml:224
#, kde-format
msgid "Browse this device"
msgstr "Browse this device"

#: package/contents/ui/DeviceDelegate.qml:241
#, kde-format
msgid "SMS Messages"
msgstr "SMS Messages"

#: package/contents/ui/DeviceDelegate.qml:262
#, kde-format
msgid "Remote Keyboard"
msgstr "Remote Keyboard"

#: package/contents/ui/DeviceDelegate.qml:278
#, kde-format
msgid "Notifications:"
msgstr "Notifications:"

#: package/contents/ui/DeviceDelegate.qml:285
#, kde-format
msgid "Dismiss all notifications"
msgstr "Dismiss all notifications"

#: package/contents/ui/DeviceDelegate.qml:324
#, kde-format
msgid "Reply"
msgstr "Reply"

#: package/contents/ui/DeviceDelegate.qml:333
#, kde-format
msgid "Dismiss"
msgstr "Dismiss"

#: package/contents/ui/DeviceDelegate.qml:344
#, kde-format
msgid "Cancel"
msgstr ""

#: package/contents/ui/DeviceDelegate.qml:357
#, kde-format
msgctxt "@info:placeholder"
msgid "Reply to %1…"
msgstr ""

#: package/contents/ui/DeviceDelegate.qml:373
#, kde-format
msgid "Send"
msgstr ""

#: package/contents/ui/DeviceDelegate.qml:397
#, kde-format
msgid "Run command"
msgstr "Run command"

#: package/contents/ui/DeviceDelegate.qml:405
#, kde-format
msgid "Add command"
msgstr "Add command"

#: package/contents/ui/FullRepresentation.qml:53
#, kde-format
msgid "No paired devices"
msgstr "No paired devices"

#: package/contents/ui/FullRepresentation.qml:53
#, kde-format
msgid "Paired device is unavailable"
msgid_plural "All paired devices are unavailable"
msgstr[0] "Paired device is unavailable"
msgstr[1] "All paired devices are unavailable"

#: package/contents/ui/FullRepresentation.qml:55
#, kde-format
msgid "Install KDE Connect on your Android device to integrate it with Plasma!"
msgstr ""
"Install KDE Connect on your Android device to integrate it with Plasma!"

#: package/contents/ui/FullRepresentation.qml:59
#, kde-format
msgid "Pair a Device..."
msgstr "Pair a Device..."

#: package/contents/ui/FullRepresentation.qml:71
#, kde-format
msgid "Install from Google Play"
msgstr "Install from Google Play"

#: package/contents/ui/FullRepresentation.qml:81
#, kde-format
msgid "Install from F-Droid"
msgstr "Install from F-Droid"

#: package/contents/ui/main.qml:49
#, kde-format
msgid "KDE Connect Settings..."
msgstr "KDE Connect Settings..."

#~ msgid "Configure..."
#~ msgstr "Configure..."

#~ msgctxt ""
#~ "Display the battery charge percentage with the label \"Battery:\" so the "
#~ "user knows what is being displayed"
#~ msgid "Battery: %1"
#~ msgstr "Battery: %1"

#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgid "Share text"
#~ msgstr "Share text"

#~ msgid "Charging: %1%"
#~ msgstr "Charging: %1%"

#~ msgid "Discharging: %1%"
#~ msgstr "Discharging: %1%"
